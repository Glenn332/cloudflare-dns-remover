from logic.CloudFlareDnsRemover import CloudFlareDnsRemover
from logic.SettingsReader import SettingsReader as SettingsReader

settings_reader = SettingsReader()
settings = settings_reader.read()

if(settings == None):
    print("Settings could not be loaded, shutting down application.")
    quit()

cloud_flare_dns_remover = CloudFlareDnsRemover(settings.cloud_flare_token, settings.zones_to_process)

print("Starting CloudFlareDnsRemoval process")

cloud_flare_dns_remover.execute()

print("Ending CloudFlareDnsRemoval process")
