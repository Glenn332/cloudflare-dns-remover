from models.CloudFlareZone import CloudFlareZone


class Settings():
    def __init__(self, CloudFlareToken, ZonesToProcess):
        self.cloud_flare_token = CloudFlareToken
        self.zones_to_process = []
        self.set_zones_to_process(ZonesToProcess)        

    def set_zones_to_process(self, zonesToProcess):
        self.zones_to_process = []
        
        for zone in zonesToProcess:
            self.zones_to_process.append(CloudFlareZone(**zone))