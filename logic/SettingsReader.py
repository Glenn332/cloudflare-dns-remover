import os
import json

from models.Settings import Settings

class SettingsReader():
    def __init__(self):
        self.settings_path = "settings.json"
        self.development_settings_path = "settings.Development.json"

    def read(self):
        fileExists = os.path.isfile(self.settings_path)

        if(fileExists == False):
            print("File does not exist")
            return None

        with open(self.settings_path) as json_file:
            data = json.load(json_file)

            settings = Settings(**data)

            self.load_development_overrides(settings)

            return settings

    def load_development_overrides(self, settings):
        fileExists = os.path.isfile(self.development_settings_path)

        if(fileExists == False):
            return

        with open(self.development_settings_path) as json_file:
            data = json.load(json_file)

            if("CloudFlareToken" in data):
                settings.cloud_flare_token = data["CloudFlareToken"]

            if("ZonesToProcess" in data):
                settings.set_zones_to_process(data["ZonesToProcess"])

        
