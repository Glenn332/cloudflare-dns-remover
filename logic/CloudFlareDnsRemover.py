import requests


class CloudFlareDnsRemover():
    def __init__(self, cloud_flare_token, zones_to_process):
        self.cloud_flare_token = cloud_flare_token
        self.zones_to_process = zones_to_process
        self.cloud_flare_get_dns_records_url = "https://api.cloudflare.com/client/v4/zones/{zone_id}/dns_records"
        self.cloud_flare_delete_dns_record = "https://api.cloudflare.com/client/v4/zones/{zone_id}/dns_records/{dns_id}"

    def execute(self):
        for zone in self.zones_to_process:
            self.handle_zone(zone)

    def handle_zone(self, zone):
        r = requests.get(self.cloud_flare_get_dns_records_url.format(
            zone_id=zone.id), headers={"Authorization": "Bearer " + self.cloud_flare_token})

        if(r.status_code != 200):
            print("Could not retrieve DNS entries for zone_id {zone_id}, stopping removal process for zone.".format(zone_id=zone.id))
            return

        for dns_entry in r.json()['result']:            
            if(dns_entry['id'] in zone.dns_ids_to_exclude):
                continue

            self.removeDnsEntry(dns_entry)

    def removeDnsEntry(self, dns_entry):
        print("Removing dns entry {Id} - {name}".format(Id=dns_entry['id'], name=dns_entry['name']))

        r = requests.delete(self.cloud_flare_delete_dns_record.format(
            zone_id=dns_entry['zone_id'], dns_id=dns_entry['id']), headers={"Authorization": "Bearer " + self.cloud_flare_token})

        if(r.status_code != 200):
            print("Could not remve DNS entry for zone_id {zone_id}, dns_id {dns_id}, stopping removal process for DNS entry.".format(zone_id=dns_entry['zone_id'], dns_id=dns_entry['id']))
            return

        print("Removed dns entry {id} - {name}".format(id=dns_entry['id'], name=dns_entry['name']))
